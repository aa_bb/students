﻿using System.Xml.Serialization;

namespace CSharpPart2Students
{
    [XmlInclude(typeof(Master))]
    public class Student
    {
        public Student() { }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Faculty { get; set; }
    }
}
