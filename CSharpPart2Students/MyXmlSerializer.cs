﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace CSharpPart2Students
{
    class MyXmlSerializer
    {
        private XmlSerializer serializer;

        private static XmlAttributeOverrides GetXmlAttributeOverrides()
        {
            XmlAttributeOverrides xmlAttrOverrides = new XmlAttributeOverrides();

            XmlAttributes xmlAttrs = new XmlAttributes();
            xmlAttrs.XmlElements.Add(new XmlElementAttribute(typeof(Master)));

            xmlAttrOverrides.Add(typeof(List<Student>), "Base List", xmlAttrs);

            return xmlAttrOverrides;
        }

        public MyXmlSerializer()
        {
            serializer = new XmlSerializer(typeof(List<Student>), GetXmlAttributeOverrides());
        }

        public bool SerializeList(List<Student> list, Stream file)
        {
            serializer.Serialize(file, list);

            return true;
        }

        public List<Student> DeserializeList(Stream file)
        {
            return (List<Student>)serializer.Deserialize(file);
        }
    }
}
