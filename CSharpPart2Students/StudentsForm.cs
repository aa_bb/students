﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace CSharpPart2Students
{
    public partial class StudentsForm : Form
    {
        List<Student> studentsCollection;
        List<Student> subStudentsCollection;
        List<Student> workCollection;
        MyXmlSerializer serializer;
        string filename = "Students.xml";
        int currentStudentsIndex = -1;
        int CurrentStudentsIndex
        {
            set
            {
                currentStudentsIndex = value;
                ChangeStudentsControls();
            }
            get { return currentStudentsIndex; }
        }

        public StudentsForm()
        {
            InitializeComponent();
            serializer = new MyXmlSerializer();
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            studentsCollection = new List<Student>();
            workCollection = studentsCollection;
            файлToolStripMenuItem.DropDownItems[2].Enabled = true; // сохранить
            студентToolStripMenuItem.Enabled = true;
            студентToolStripMenuItem.DropDownItems[1].Enabled = false; // удалить
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var file = new FileStream(filename, FileMode.Open))
                studentsCollection = serializer.DeserializeList(file);
            workCollection = studentsCollection;
            студентToolStripMenuItem.Enabled = true;
            CurrentStudentsIndex = 0;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var file = new FileStream(filename, FileMode.Create))
                serializer.SerializeList(studentsCollection, file);
        }

        private void предыдущийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentStudentsIndex--;
        }

        private void следующийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentStudentsIndex++;
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = MessageBox.Show("Студент - магистр?", "Магистр?", MessageBoxButtons.YesNoCancel);
            if (f.ToString() == "Cancel")
                return;
            Student student;
            if (f.ToString() == "Yes")
                student = new Master();
            else
                student = new Student();
            studentsCollection.Add(student);
            CurrentStudentsIndex = studentsCollection.Count - 1;
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            studentsCollection.RemoveAt(CurrentStudentsIndex);
            CurrentStudentsIndex--;
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            CurrentStudentsIndex--;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            CurrentStudentsIndex++;
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            workCollection[CurrentStudentsIndex].Name = nameTextBox.Text;
        }

        private void surnameTextBox_TextChanged(object sender, EventArgs e)
        {
            workCollection[CurrentStudentsIndex].Surname = surnameTextBox.Text;
        }

        private void facultyTextBox_TextChanged(object sender, EventArgs e)
        {
            workCollection[CurrentStudentsIndex].Faculty = facultyTextBox.Text;
        }

        private void diplomaCodeTextBox_TextChanged(object sender, EventArgs e)
        {
            ((Master) workCollection[CurrentStudentsIndex]).DiplomaCode = diplomaCodeTextBox.Text;
        }

        private void makeMasterButton_Click(object sender, EventArgs e)
        {
            Master newMaster = new Master();
            newMaster.Name = workCollection[CurrentStudentsIndex].Name;
            newMaster.Surname = workCollection[CurrentStudentsIndex].Surname;
            newMaster.Faculty = workCollection[CurrentStudentsIndex].Faculty;
            workCollection[CurrentStudentsIndex] = newMaster;
            ChangeStudentsControls();
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if (searchComboBox.SelectedIndex == -1)
                searchComboBox.SelectedIndex = 0;
            else
                Search();
        }

        private void searchComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Search();
        }

        public List<Control> GetControls<T>(Control control)
        {
            List<Control> control_list = new List<Control>();
            foreach (Control c in control.Controls)
            {
                if (c is T)
                {
                    control_list.Add(c);
                    control_list.AddRange(GetControls<T>(c));
                }
            }
            return control_list;
        }

        List<Control> GetInputSearchControls()
        {
            List<Control> ctrls = GetControls<TextBox>(this);
            ctrls.AddRange(GetControls<ComboBox>(this));
            ctrls.AddRange(GetControls<Label>(this));
            return ctrls;
        }

        void CheckBlockMoveDeleteControls()
        {
            List<Control> inputSearchControls = GetInputSearchControls();
            if (CurrentStudentsIndex < 1)
            {
                просмотрToolStripMenuItem.DropDownItems[0].Enabled = false; // предыдущий
                previousButton.Enabled = false;
            }
            else
            {
                просмотрToolStripMenuItem.DropDownItems[0].Enabled = true; // предыдущий
                previousButton.Enabled = true;
            }

            if (CurrentStudentsIndex == workCollection.Count - 1)
            {
                просмотрToolStripMenuItem.DropDownItems[1].Enabled = false; // следующий
                nextButton.Enabled = false;
            }
            else
            {
                просмотрToolStripMenuItem.DropDownItems[1].Enabled = true; // следующий
                nextButton.Enabled = true;
            }

            if (workCollection.Count == 0)
            {
                BlockInputSearchControls(inputSearchControls);
                студентToolStripMenuItem.DropDownItems[1].Enabled = false; // удалить
                makeMasterButton.Enabled = false;
            }
            else
            {
                UnblockInputSeatchControls(inputSearchControls);
                студентToolStripMenuItem.DropDownItems[1].Enabled = true; // удалить
                makeMasterButton.Enabled = true;
            }
        }

        void ChangeStudentsControls()
        {
            CheckBlockMoveDeleteControls();
            if (CurrentStudentsIndex == -1 || workCollection.Count == 0)
                return;

            if (workCollection[CurrentStudentsIndex] is Master)
            {
                makeMasterButton.Visible = false;
                diplomaCodeLabel.Visible = true;
                diplomaCodeTextBox.Visible = true;
            }
            else
            {
                makeMasterButton.Visible = true;
                makeMasterButton.Enabled = true;
                diplomaCodeLabel.Visible = false;
                diplomaCodeTextBox.Visible = false;
            }
            
            ChangeTextBoxes();
        }

        void ChangeTextBoxes()
        {
            if (string.IsNullOrEmpty(searchTextBox.Text))
            {
                nameTextBox.Text = workCollection[currentStudentsIndex].Name;
                surnameTextBox.Text = workCollection[currentStudentsIndex].Surname;
                facultyTextBox.Text = workCollection[currentStudentsIndex].Faculty;
                if (workCollection[currentStudentsIndex] is Master)
                    diplomaCodeTextBox.Text = ((Master)workCollection[currentStudentsIndex]).DiplomaCode;
            }
            else
            {
                nameTextBox.Text = workCollection[currentStudentsIndex].Name;
                surnameTextBox.Text = workCollection[currentStudentsIndex].Surname;
                facultyTextBox.Text = workCollection[currentStudentsIndex].Faculty;
                if (workCollection[currentStudentsIndex] is Master)
                    diplomaCodeTextBox.Text = ((Master)workCollection[currentStudentsIndex]).DiplomaCode;
            }
        }

        void BlockInputSearchControls(List<Control> ctrls)
        {
            foreach (var ctrl in ctrls)
                ctrl.Enabled = false;
            searchComboBox.Enabled = true;
            searchTextBox.Enabled = true;
        }

        void UnblockInputSeatchControls(List<Control> ctrls)
        {
            foreach (var ctrl in ctrls)
                ctrl.Enabled = true;
        }

        void Search()
        {
            if (string.IsNullOrEmpty(searchTextBox.Text))
            {
                workCollection = studentsCollection;
                CurrentStudentsIndex = 0;
                return;
            }

            IEnumerable<Student> tmp;
            if (searchComboBox.SelectedItem.ToString() == "Имя")
                tmp = studentsCollection.Where(s => s.Name == searchTextBox.Text);
            else if (searchComboBox.SelectedItem.ToString() == "Фамилия")
                tmp = studentsCollection.Where(s => s.Surname == searchTextBox.Text);
            else
                tmp = studentsCollection.Where(s => s.Faculty == searchTextBox.Text);

            subStudentsCollection = tmp.ToList();
            workCollection = subStudentsCollection;
            if (workCollection.Count == 0)
                CurrentStudentsIndex = -1;
            else
                CurrentStudentsIndex = 0;
        }
    }
}
